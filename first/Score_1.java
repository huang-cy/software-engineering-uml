package first;

import com.sun.xml.internal.bind.v2.model.core.EnumConstant;
import org.jsoup.Jsoup;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.Properties;

public class Score {
    public static void main(String s[]) throws Exception{
        Document allDocument = Jsoup.parse(new File("src\\first\\all.html"),"UTF-8");
        Document smallDocument = Jsoup.parse(new File("src\\first\\small.html"),"UTF-8");
        total(beforeSum(allDocument,smallDocument),baseSum(allDocument,smallDocument),testSum(allDocument,smallDocument)
        ,programSum(allDocument,smallDocument),addSum(allDocument,smallDocument));

    }
    static int beforeSum(Document allDocument,Document smallDocument){
        Elements allElement = allDocument.select("[data-type=QUIZ]");
        Elements smallElement = smallDocument.select("[data-type=QUIZ]");
        int sumScore = 0;
        for(int i = 0;i < allElement.size();i++){
            Element all = allElement.get(i).child(1).child(2).child(0).child(10);
            String str = all.text().replace(" 经验","");
            sumScore += Integer.parseInt(str);
        }
        for(int i = 0;i < smallElement.size();i++){
            Element small = smallElement.get(i).child(1).child(2).child(0).child(10);
            String str1 = small.text().replace(" 经验","");
            sumScore += Integer.parseInt(str1);
        }
        return sumScore;
    }
    static int baseSum(Document allDocument,Document smallDocument){
        Elements allElement = allDocument.select("[data-appraise-type=APPRAISER]");
        Elements smallElement = smallDocument.select("[data-appraise-type=APPRAISER]");
        int sumScore = 0;
        for(int i = 0;i < allElement.size();i++){
            Element isTrue = allElement.get(i).child(1).child(0).child(1);
            if(isTrue.text().contains("课堂完成部分")) {
                Element isJoin = allElement.get(i).child(1).child(2).child(0).child(6);
                if (isJoin.text().contains("已参与")) {
                    Element Score = allElement.get(i).child(1).child(2).child(0).child(7);
                    String str = Score.text().replace(" 经验", "");
                    sumScore += Integer.parseInt(str);
                }
            }
        }
        for(int i = 0;i < smallElement.size();i++){
            Element isTrue1 = smallElement.get(i).child(1).child(0).child(1);
            if(isTrue1.text().contains("课堂完成部分")) {
                Element isJoin1 = smallElement.get(i).child(1).child(2).child(0).child(6);
                if (isJoin1.text().contains("已参与")) {
                    Element Score1 = smallElement.get(i).child(1).child(2).child(0).child(7);
                    String str1 = Score1.text().replace(" 经验", "");
                    sumScore += Integer.parseInt(str1);
                }
            }
        }
        return sumScore;
    }
    static int testSum(Document allDocument,Document smallDocument){
        Elements allElement = allDocument.select("[data-appraise-type=APPRAISER]");
        Elements smallElement = smallDocument.select("[data-appraise-type=APPRAISER]");
        int sumScore = 0;
        for(int i = 0;i < allElement.size();i++) {
            Element isTrue = allElement.get(i).child(1).child(0).child(1);
            if (isTrue.text().contains("课堂小测")) {
                Element isJoin = allElement.get(i).child(1).child(2).child(0).child(6);
                if (isJoin.text().contains("已参与")) {
                    Element Score = allElement.get(i).child(1).child(2).child(0).child(7);
                    String str = Score.text().replace(" 经验", "");
                    sumScore += Integer.parseInt(str);
                }
            }
        }
        for(int i = 0;i < smallElement.size();i++){
            Element isTrue1 = smallElement.get(i).child(1).child(0).child(1);
            if(isTrue1.text().contains("课堂小测")) {
                Element isJoin1 = smallElement.get(i).child(1).child(2).child(0).child(6);
                if (isJoin1.text().contains("已参与")) {
                    Element Score1 = smallElement.get(i).child(1).child(2).child(0).child(7);
                    String str1 = Score1.text().replace(" 经验", "");
                    sumScore += Integer.parseInt(str1);
                }
            }
        }
        return sumScore;
    }
    static int programSum(Document allDocument,Document smallDocument){
        Elements allElement = allDocument.select("[data-appraise-type=TEACHER]");
        Elements smallElement = smallDocument.select("[data-appraise-type=TEACHER]");
        int sumScore = 0;
        for(int i = 0;i < allElement.size();i++) {
            Element isTrue = allElement.get(i).child(1).child(0).child(1);
            if (isTrue.text().contains("编程题")) {
                Element isJoin = allElement.get(i).child(1).child(2).child(0).child(6);
                if (isJoin.text().contains("已参与")) {
                    Element Score = allElement.get(i).child(1).child(2).child(0).child(7);
                    String str = Score.text().replace(" 经验", "");
                    sumScore += Integer.parseInt(str);
                }
            }
        }
        for(int i = 0;i < smallElement.size();i++){
            Element isTrue1 = smallElement.get(i).child(1).child(0).child(1);
            if(isTrue1.text().contains("编程题")) {
                Element isJoin1 = smallElement.get(i).child(1).child(2).child(0).child(6);
                if (isJoin1.text().contains("已参与")) {
                    Element Score1 = smallElement.get(i).child(1).child(2).child(0).child(7);
                    String str1 = Score1.text().replace(" 经验", "");
                    sumScore += Integer.parseInt(str1);
                }
            }
        }
        return sumScore;
    }
    static int addSum(Document allDocument,Document smallDocument){
        Elements allElement = allDocument.select("[data-appraise-type=TEACHER]");
        Elements smallElement = smallDocument.select("[data-appraise-type=TEACHER]");
        int sumScore = 0;
        for(int i = 0;i < allElement.size();i++) {
            Element isTrue = allElement.get(i).child(1).child(0).child(1);
            if (isTrue.text().contains("附加题")) {
                Element isJoin = allElement.get(i).child(1).child(2).child(0).child(6);
                if (isJoin.text().contains("已参与")) {
                    Element Score = allElement.get(i).child(1).child(2).child(0).child(7);
                    String str = Score.text().replace(" 经验", "");
                    sumScore += Integer.parseInt(str);
                }
            }
        }
        for(int i = 0;i < smallElement.size();i++){
            Element isTrue1 = smallElement.get(i).child(1).child(0).child(1);
            if(isTrue1.text().contains("附加题")) {
                Element isJoin1 = smallElement.get(i).child(1).child(2).child(0).child(6);
                if (isJoin1.text().contains("已参与")) {
                    Element Score1 = smallElement.get(i).child(1).child(2).child(0).child(7);
                    String str1 = Score1.text().replace(" 经验", "");
                    sumScore += Integer.parseInt(str1);
                }
            }
        }
        return sumScore;
    }
    static void total(int beforeSum,int baseSum,int testSum,int programSum,int addSum) throws Exception{
        int beforeTotal = 0;
        int baseTotal = 0;
        int testTotal = 0;
        int programTotal = 0;
        int addTotal = 0;
        Properties con = new Properties();
        con.load(new FileInputStream("src\\first\\total.properties"));
        Enumeration<?> fileName = con.propertyNames();
        while(fileName.hasMoreElements()){
            String strKey = (String)fileName.nextElement();
            if(strKey.equals("before")) {
                beforeTotal = Integer.parseInt(con.getProperty(strKey));
            }
            else if(strKey.equals("base")){
                baseTotal = Integer.parseInt(con.getProperty(strKey));
            }
            else if(strKey.equals("test")){
                testTotal = Integer.parseInt(con.getProperty(strKey));
            }
            else if(strKey.equals("program")){
                programTotal = Integer.parseInt(con.getProperty(strKey));
            }
            else if(strKey.equals("add")){
                addTotal = Integer.parseInt(con.getProperty(strKey));
            }
        }
        double totalScore = (baseSum / baseTotal * 1.0) * 95 *0.3 + (programSum / programTotal * 1.0) * 95 * 0.1 + (addSum / addTotal * 1.0) * 90 * 0.05
                + (beforeSum / beforeTotal * 1.0) * 100 * 0.25 + (testSum / testTotal * 1.0) * 100 * 0.2;
        System.out.printf("%.2f",totalScore + 6);
    }

}
