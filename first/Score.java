package first;

import com.sun.xml.internal.bind.v2.model.core.EnumConstant;
import org.jsoup.Jsoup;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Scanner;

public class Score {
    public static void main(String s[]) throws Exception{
        Document allDocument = Jsoup.parse(new File("UML/src/first/all.html"),"UTF-8");
        Document smallDocument = Jsoup.parse(new File("UML/src/first/small.html"),"UTF-8");
        int before = beforeSum(allDocument,smallDocument);          //课前自测
        int base = baseSum(allDocument,smallDocument);              //课堂完成
        int test = testSum(allDocument,smallDocument);              //课堂小测
        int program =programSum(allDocument,smallDocument);         //编程题
        int add = addSum(allDocument,smallDocument);                //附加题
        total(before,base,test,program,add);                        //计算总分

    }
    //课前自测分数计算
    static int beforeSum(Document allDocument,Document smallDocument){
        Elements allElement = allDocument.getElementsByClass("interaction-row");
        Elements smallElement = smallDocument.getElementsByClass("interaction-row");
        int sumScore = 0;
        //计算 all.html 文件中 课前自测的分数
        for(int i = 0;i < allElement.size();i++){
            String QuestionType = allElement.get(i).toString();
            if(QuestionType.contains("课前自测")){
                Elements span = allElement.get(i).getElementsByTag("span");
                for(int j = 0;j < span.size();j++){
                    if(span.get(j).toString().contains("经验")){
                        Scanner sc = new Scanner(span.get(j).text());
                        sumScore += sc.nextInt();
                        break;
                    }
                }
            }
        }
        //计算 small.html 文件中 课前自测的分数
        for(int i = 0;i < smallElement.size();i++){
            String QuestionType = smallElement.get(i).toString();
            if(QuestionType.contains("课前自测")){
                Elements span = smallElement.get(i).getElementsByTag("span");
                for(int j = 0;j < span.size();j++){
                    if(span.get(j).toString().contains("经验")){
                        Scanner sc = new Scanner(span.get(j).text());
                        sumScore += sc.nextInt();
                        break;
                    }
                }
            }
        }
        return sumScore;
    }
    // 计算 课堂完成部分的分数
    static int baseSum(Document allDocument,Document smallDocument){
        Elements allElement = allDocument.getElementsByClass("interaction-row");
        Elements smallElement = smallDocument.getElementsByClass("interaction-row");
        int sumScore = 0;
        //计算 all.html 文件中 课前自测的分数
        for(int i = 0;i < allElement.size();i++){
            String QuestionType = allElement.get(i).toString();
            if(QuestionType.contains("课堂完成") && QuestionType.contains("已参与")) {
                Elements span = allElement.get(i).getElementsByTag("span");
                for(int j = 0;j < span.size();j++){
                    if(span.get(j).toString().contains("经验")){
                        Scanner sc = new Scanner(span.get(j).text());
                        sumScore += sc.nextInt();
                        break;
                    }
                }
            }
        }
        //计算 small.html 文件中 课前自测的分数
        for(int i = 0;i < smallElement.size();i++){
            String QuestionType = smallElement.get(i).toString();
            if(QuestionType.contains("课堂完成") && QuestionType.contains("已参与")) {
                Elements span = smallElement.get(i).getElementsByTag("span");
                for(int j = 0;j < span.size();j++){
                    if(span.get(j).toString().contains("经验")){
                        Scanner sc = new Scanner(span.get(j).text());
                        sumScore += sc.nextInt();
                        break;
                    }
                }
            }
        }
        return sumScore;
    }
    //计算 课堂小测的分数
    static int testSum(Document allDocument,Document smallDocument){
        Elements allElement = allDocument.getElementsByClass("interaction-row");
        Elements smallElement = smallDocument.getElementsByClass("interaction-row");
        int sumScore = 0;
        //计算 all.html 文件中 课前自测的分数
        for(int i = 0;i < allElement.size();i++){
            String QuestionType = allElement.get(i).toString();
            if(QuestionType.contains("课堂小测") && QuestionType.contains("已参与")) {
                Elements span = allElement.get(i).getElementsByTag("span");
                for(int j = 0;j < span.size();j++){
                    if(span.get(j).toString().contains("经验")){
                        Scanner sc = new Scanner(span.get(j).text());
                        sumScore += sc.nextInt();
                        break;
                    }
                }
            }
        }
        //计算 small.html 文件中 课前自测的分数
        for(int i = 0;i < smallElement.size();i++){
            String QuestionType = smallElement.get(i).toString();
            if(QuestionType.contains("课堂小测") && QuestionType.contains("已参与")) {
                Elements span = smallElement.get(i).getElementsByTag("span");
                for(int j = 0;j < span.size();j++){
                    if(span.get(j).toString().contains("经验")){
                        Scanner sc = new Scanner(span.get(j).text());
                        sumScore += sc.nextInt();
                        break;
                    }
                }
            }
        }
        return sumScore;
    }
    //计算编程题的分数
    static int programSum(Document allDocument,Document smallDocument){
        Elements allElement = allDocument.getElementsByClass("interaction-row");
        Elements smallElement = smallDocument.getElementsByClass("interaction-row");
        int sumScore = 0;
        //计算 all.html 文件中 课前自测的分数
        for(int i = 0;i < allElement.size();i++){
            String QuestionType = allElement.get(i).toString();
            if(QuestionType.contains("编程题") && QuestionType.contains("已参与")) {
                Elements span = allElement.get(i).getElementsByTag("span");
                for(int j = 0;j < span.size();j++){
                    if(span.get(j).toString().contains("经验")){
                        Scanner sc = new Scanner(span.get(j).text());
                        sumScore += sc.nextInt();
                        break;
                    }
                }
            }
        }
        //计算 small.html 文件中 课前自测的分数
        for(int i = 0;i < smallElement.size();i++){
            String QuestionType = smallElement.get(i).toString();
            if(QuestionType.contains("编程题") && QuestionType.contains("已参与")) {
                Elements span = smallElement.get(i).getElementsByTag("span");
                for(int j = 0;j < span.size();j++){
                    if(span.get(j).toString().contains("经验")){
                        Scanner sc = new Scanner(span.get(j).text());
                        sumScore += sc.nextInt();
                        break;
                    }
                }
            }
        }
        return sumScore;
    }
    //计算附加题的分数
    static int addSum(Document allDocument,Document smallDocument){
        Elements allElement = allDocument.getElementsByClass("interaction-row");
        Elements smallElement = smallDocument.getElementsByClass("interaction-row");
        int sumScore = 0;
        //计算 all.html 文件中 课前自测的分数
        for(int i = 0;i < allElement.size();i++){
            String QuestionType = allElement.get(i).toString();
            if(QuestionType.contains("附加题") && QuestionType.contains("已参与")) {
                System.out.println(allElement.get(i));
                Elements span = allElement.get(i).getElementsByTag("span");
                for(int j = 0;j < span.size();j++){
                    if(span.get(j).toString().contains("经验")){
                        Scanner sc = new Scanner(span.get(j).text());
                        sumScore += sc.nextInt();
                        break;
                    }
                }
            }
        }
        //计算 small.html 文件中 课前自测的分数
        for(int i = 0;i < smallElement.size();i++){
            String QuestionType = smallElement.get(i).toString();
            if(QuestionType.contains("附加题") && QuestionType.contains("已参与")) {
                Elements span = smallElement.get(i).getElementsByTag("span");
                for(int j = 0;j < span.size();j++){
                    if(span.get(j).toString().contains("经验")){
                        Scanner sc = new Scanner(span.get(j).text());
                        sumScore += sc.nextInt();
                        break;
                    }
                }
            }
        }
        return sumScore;
    }
    static void total(int beforeSum,int baseSum,int testSum,int programSum,int addSum) throws Exception{
        int beforeTotal = 0;
        int baseTotal = 0;
        int testTotal = 0;
        int programTotal = 0;
        int addTotal = 0;
        //定义五个变量

        Properties con = new Properties();
        con.load(new FileInputStream("UML/src/first/total.properties"));
        Enumeration fileName = con.propertyNames();
        //加载配置 properties 文件

        beforeTotal = Integer.parseInt(con.getProperty("before"));      //读取课前自测总分
        baseTotal = Integer.parseInt(con.getProperty("base"));          //读取课堂完成总分
        testTotal = Integer.parseInt(con.getProperty("test"));          //读取课堂小测总分
        programTotal = Integer.parseInt(con.getProperty("program"));    //读取编程题总分
        addTotal = Integer.parseInt(con.getProperty("add"));            //读取附加题总分
        System.out.println(beforeTotal);


        double totalScore = (baseSum / baseTotal * 1.0) * 95 *0.3 + (programSum / programTotal * 1.0) * 95 * 0.1 + (addSum / addTotal * 1.0) * 90 * 0.05
                + (beforeSum / beforeTotal * 1.0) * 100 * 0.25 + (testSum / testTotal * 1.0) * 100 * 0.2 + 6;
        //计算应该得到的分数。

        System.out.printf("%.2f",totalScore);
    }

}
